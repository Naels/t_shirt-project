import { proxy } from "valtio";

const state = proxy({
    intro: true,
    color: '#EFBD48',
    isLogoTexture: true,
    isFullTexture: false,
    logoDecal: './nsa-logo.png',
    fullDecal: './nsa-logo.png'
})

export default state